// Copyright (c) 2000-2013 Viaccess SA.
// Les Collines de l Arche - Tour Opera C, 92057 Paris La Defense, FRANCE
// All rights reserved.
//
// This software is the confidential and proprietary information of
// Viaccess SA. (Confidential Information). You shall not
// disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with Viaccess.
package tstclient;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

public class TestClient {
    public static void main(final String[] args) throws Exception {
        int nbCall = 200;
        if ((args != null) && (args.length > 0)) {
            final String arg0 = args[0];
            nbCall = Integer.parseInt(arg0);
        }
        final TestClient http = new TestClient();

        final HttpTestClient client = http.new HttpTestClient();

        final ExecutorService executorService = Executors.newFixedThreadPool(50);

        final List<Future<Long>> futures = new LinkedList();
        for (int i = 0; i < nbCall; i++) {
            futures.add(executorService.submit(client));
        }
        executorService.shutdown();

        doStat(nbCall, futures);
    }

    private static void doStat(final int nbCall, final List<Future<Long>> futures) throws InterruptedException, ExecutionException {
        Long totalTime = 0L;
        Long maxTime = 0L;
        Long minTime = 0L;
        for (final Future<Long> future : futures) {
            final Long current = future.get();
            totalTime += current;

            if (current > maxTime) {
                maxTime = current;
            }
            if ((current < minTime) || (minTime == 0L)) {
                minTime = current;
            }
        }
        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("e:/myfile.txt", true)))) {
            out.println("-------------------");
            out.println("nb call : " + nbCall);
            out.println("avg time: " + ((totalTime / nbCall)));
            out.println("min time: " + minTime);
            out.println("max time: " + maxTime);
        } catch (final IOException e) {
            System.out.println(e);
        }

        System.out.println("nb call : " + nbCall);
        System.out.println("avg time: " + ((totalTime / nbCall)));
        System.out.println("min time: " + minTime);
        System.out.println("max time: " + maxTime);
    }

    public class HttpTestClient implements Callable<Long> {

        final CloseableHttpClient client;
        final String url = "http://localhost:8090/test/test";

        public HttpTestClient() {
            super();
            this.client = HttpClients.createDefault();
        }

        public void sendGet() throws Exception {

            final HttpGet request = new HttpGet(this.url);
            final HttpResponse response = this.client.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.println("KO");
            }
        }

        @Override
        public Long call() throws Exception {
            final long currentTimeMillis = System.currentTimeMillis();
            try {
                this.sendGet();
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return System.currentTimeMillis() - currentTimeMillis;
        }
    }

}
